﻿using Inventor;
using System;
using System.Runtime.InteropServices;

namespace InventorSketches3D
{
    public class InventorExtention
    {
        /// <summary>
        /// 获取InventorApplication对象
        /// </summary>
        /// <returns>InventorApplication对象</returns>
        public static Application GetInventorApp()
        {
            Inventor.Application inventorApp = null;
            try
            {
                inventorApp = Marshal.GetActiveObject("Inventor.Application") as Inventor.Application;
            }
            catch
            {
                var inventorType = Type.GetTypeFromProgID("Inventor.Application");
                inventorApp = Activator.CreateInstance(inventorType) as Inventor.Application;
                inventorApp.Visible = true;
            }
            return inventorApp;
        }
    }
}
