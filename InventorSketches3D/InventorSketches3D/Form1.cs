﻿using Inventor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InventorSketches3D
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var inventorApp = InventorExtention.GetInventorApp();
            // 创建ipt零件文件
            PartDocument partDoc = inventorApp.Documents.Add(
            DocumentTypeEnum.kPartDocumentObject,
            inventorApp.FileManager.GetTemplateFile(DocumentTypeEnum.kPartDocumentObject, SystemOfMeasureEnum.kDefaultSystemOfMeasure, DraftingStandardEnum.kDefault_DraftingStandard, null),
            true) as PartDocument;
            var compDef = partDoc.ComponentDefinition;
            var tranGeo = inventorApp.TransientGeometry;

            // 三维几何点
            Inventor.Point[] pts = {
                tranGeo.CreatePoint(0, 0, 0),
                tranGeo.CreatePoint(10, 0, 0),
                tranGeo.CreatePoint(10, 10, 0),
                tranGeo.CreatePoint(0, 10, 0),
                tranGeo.CreatePoint(0, 0, 10),
                tranGeo.CreatePoint(10, 0, 10),
                tranGeo.CreatePoint(10, 10, 10)
            };
            // 三维草图
            var pathSketch = compDef.Sketches3D.Add();
            // 添加直线
            var line1 = pathSketch.SketchLines3D.AddByTwoPoints(pts[0], pts[1]);
            var line2 = pathSketch.SketchLines3D.AddByTwoPoints(pts[1], pts[2]);
            var line3 = pathSketch.SketchLines3D.AddByTwoPoints(pts[2], pts[3]);
            var line4 = pathSketch.SketchLines3D.AddByTwoPoints(pts[3], pts[4]);
            var line5 = pathSketch.SketchLines3D.AddByTwoPoints(pts[4], pts[5]);
            var line6 = pathSketch.SketchLines3D.AddByTwoPoints(pts[5], pts[6]);
            // 将线进行重合约束
            line1.EndSketchPoint.ConnectTo(line2.StartSketchPoint);
            line2.EndSketchPoint.ConnectTo(line3.StartSketchPoint);
            line3.EndSketchPoint.ConnectTo(line4.StartSketchPoint);
            line4.EndSketchPoint.ConnectTo(line5.StartSketchPoint);
            line5.EndSketchPoint.ConnectTo(line6.StartSketchPoint);
            // 添加圆角
            pathSketch.SketchArcs3D.AddAsBend(line1, line2, 2);
            pathSketch.SketchArcs3D.AddAsBend(line2, line3, 2);
            pathSketch.SketchArcs3D.AddAsBend(line3, line4, 2);
            pathSketch.SketchArcs3D.AddAsBend(line4, line5, 2);
            pathSketch.SketchArcs3D.AddAsBend(line5, line6, 2);
        }


    }
}
